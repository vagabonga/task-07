DROP database IF EXISTS test2db;

CREATE database IF NOT EXISTS test2db;

USE test2db;

CREATE TABLE users (
                       id INT PRIMARY KEY AUTO_INCREMENT,
                       login VARCHAR(10),
                       UNIQUE(login)
);

CREATE TABLE teams (
                       id INT PRIMARY KEY AUTO_INCREMENT,
                       name VARCHAR(10)
);

CREATE TABLE users_teams (
                             user_id INT,
                             team_id INT,
                             FOREIGN KEY (user_id) REFERENCES users(id) on delete cascade,
                             FOREIGN KEY (team_id) REFERENCES teams(id) on delete cascade
);

--INSERT INTO users VALUES (DEFAULT, 'ivanov');
--INSERT INTO teams VALUES (DEFAULT, 'teamA');

SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;

