package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.mysql.cj.x.protobuf.MysqlxPrepare;


public class DBManager {
	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> usrs = new ArrayList<>();

		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM users ORDER BY id;")) {
			while(rs.next()) {
				User u = User.createUser(rs.getString("login"));
				u.setId(rs.getInt("id"));
				usrs.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return usrs;
	}

	public boolean insertUser(User user) throws DBException {
		if(user == null) return false;

		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			PreparedStatement stmt = con.prepareStatement("INSERT INTO users (login) VALUES(?);");) {
			stmt.setString(1,  user.getLogin());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;

	}

	public boolean deleteUsers(User... users) throws DBException {
		if(users.length == 0) return false;
		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME,  Connecting.PASSWORD);
			PreparedStatement stmt = con.prepareStatement("DELETE FROM users WHERE login = ?");) {
			for (User it : users) {
				stmt.setString(1, it.getLogin());
				stmt.executeUpdate();
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User u = new User();
		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			PreparedStatement stmt = con.prepareStatement("SELECT id, login FROM users WHERE login = ?;");) {

			stmt.setString(1, login);

			try(ResultSet rs = stmt.executeQuery();) {
				while(rs.next()) {
					u.setLogin(rs.getString("login"));
					u.setId(rs.getInt("id"));
				}
			}

		} catch(SQLException e){
			e.printStackTrace();
		}
		return u;
	}

	public Team getTeam(String name) throws DBException {
		Team t = new Team();
		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			PreparedStatement stmt = con.prepareStatement("SELECT id, name FROM teams WHERE name = ?;");) {
			stmt.setString(1, name);

			try(ResultSet rs = stmt.executeQuery();) {
				while(rs.next()) {
					t.setId(rs.getInt("id"));
					t.setName(rs.getString("name"));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return t;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> tms = new ArrayList<>();
		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			PreparedStatement stmt = con.prepareStatement("SELECT * FROM teams;");
			ResultSet rs = stmt.executeQuery();) {

			while(rs.next()) {
				Team t = Team.createTeam(rs.getString("name"));
				t.setId(rs.getInt("id"));
				tms.add(t);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return tms;
	}

	public boolean insertTeam(Team team) throws DBException {
		if(team == null) return false;

		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			PreparedStatement stmt = con.prepareStatement("INSERT INTO teams(name) VALUES(?);");) {
			stmt.setString(1, team.getName());

			stmt.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			addUser(user, con);

			for (Team it : teams) {
				addUsersTeams(con, getUser(user.getLogin()).getId(), getTeam(it.getName()).getId());
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
		} finally {
			close(con);
		}
		/*
		Connection con = null;
		PreparedStatement pstmt = null;

		user.setId(getUser(user.getLogin()).getId());
		try {
			con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			pstmt = con.prepareStatement("INSERT INTO users_teams(user_id, team_id) VALUES(?, ?);");

			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			for (Team t : teams) {
				t.setId(getTeam(t.getName()).getId());
				pstmt.setInt(1, user.getId());
				pstmt.setInt(2, t.getId());
				pstmt.executeUpdate();
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
		} finally {
			close(pstmt);
			close(con);
		}
*/
		return true;
	}
	private void addUser(User user, Connection con) {
		try (PreparedStatement pstmt = con.prepareStatement("INSERT INTO users(login) VALUES(?);",
				Statement.RETURN_GENERATED_KEYS);) {
			int k = 0;

			pstmt.setString(++k, user.getLogin());

			int count = pstmt.executeUpdate();

			if (count > 0) {
				try (ResultSet rs = pstmt.getGeneratedKeys();) {
					if (rs.next()) user.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private void addUsersTeams(Connection con, long usersId, long teamsId) {
		try (PreparedStatement pstmt = con.prepareStatement("INSERT INTO users_teams(user_id, team_id) VALUES(?, ?);");) {
			int k = 0;
			pstmt.setLong(++k, usersId);
			pstmt.setLong(++k, teamsId);

			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> tms = new ArrayList<>();
		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			PreparedStatement stmt = con.prepareStatement("SELECT name " +
					"FROM users INNER JOIN users_teams ON users.id = users_teams.user_id" +
					" INNER JOIN teams ON teams.id = users_teams.team_id " +
					"WHERE users.login = ?;");) {
			stmt.setString(1, user.getLogin());

			try(ResultSet rs = stmt.executeQuery();) {
				while(rs.next()) {
					Team t = new Team();
					t.setName(rs.getString("name"));
					tms.add(t);
				}
			}

		} catch(SQLException e) {
			e.printStackTrace();
		}
		return tms;
	}
	public boolean deleteTeam(Team team) throws DBException {
		if(team == null) return false;

		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			PreparedStatement pstmt = con.prepareStatement("DELETE FROM teams WHERE teams.name = ?");) {

			pstmt.setString(1, team.getName());
			pstmt.executeUpdate();

		} catch(SQLException e) {
			e.printStackTrace();
		}

		return true;
	}
	public boolean updateTeam(Team team) throws DBException {
		if(team == null) return false;

		try(Connection con = DriverManager.getConnection(Connecting.URL, Connecting.USERNAME, Connecting.PASSWORD);
			PreparedStatement pstmt = con.prepareStatement("UPDATE teams " +
					"SET teams.name = ? WHERE teams.id = ?;")) {
			pstmt.setString(1, team.getName());
			pstmt.setInt(2, team.getId());

			pstmt.executeUpdate();

		} catch(SQLException e) {
			e.printStackTrace();
		}

		return true;
	}
	private void rollback(Connection con) {
		try {
			con.rollback();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	private void close(AutoCloseable con) {
		if(con != null) {
			try {
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
